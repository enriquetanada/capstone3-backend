const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MountainSchema = new Schema({
	name:{
		type: String,
		unique: true,
		required: [true, 'Name field required']
	},
	rate:{
		type: Number,
		required: [true, 'Price field required'],
		min: 0.01
	},
	description:{
		type: String,
		required: [true, 'Information field required']
	},
	information:{
		type: String,
		required: [true, 'Information field required']
	},
	itinerary:{
		type: String,
		required: [true, 'Itinerary field required']
	},
	image:{
		type: String,
		required: [true, 'Image field required']
	}
}, {
	timestamps: true
})

module.exports = mongoose.model('Mountain', MountainSchema)