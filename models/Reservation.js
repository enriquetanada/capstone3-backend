const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ReservationSchema = new Schema({
	
	customerId:
	{
		type:mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: [true, "Customer ID is required"]
	},
	total: 
	{
		type: Number,
		required: [true, "Total should not be empty"],
		min: 0.01
	},
	isComplete:
	{
		type: Boolean,
		default:false
	},
	orders:
	[
		{
			mountainId: 
			{
				type: mongoose.Schema.Types.ObjectId,
				ref: 'Mountain'
			},
			person: 
			{
				type:Number,
				required: [true, "person required"]
			},
			rate:
			{
				type: Number,
				required: [true, "Rate is required"]
			},
			subtotal:
			{
				type:Number,
				required:[true, "Subtotal required"]
			}
		}
	]
},
{timestamps:true})

module.exports = mongoose.model('Reservation', ReservationSchema);