const router = require ('express').Router();
const User = require('./../models/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const passport = require('passport');

require('./../passport-setup');
router.post('/register', (req,res,next)=>
{

	let {password, confirmPassword} = req.body;
	if(password !== confirmPassword)
	{
		return res.status(400).send({
			error:  "Password and confirm password do not match"
		})
	}
	if(password.length < 8)
	{
		return res.status(400).send({
			error:"Password should be at least 8 characters"
		})
	}
	const saltRounds = 10;

	bcrypt.genSalt(saltRounds, function(err, salt)
	{
		bcrypt.hash(password, salt, function(err, hash)
		{
			req.body.password = hash;
			User.create(req.body)
			.then(user => res.send(user))
			.catch(next);
		})
	})

})

router.post('/login', (req,res,next)=>
{
	let {email, password} = req.body
	if(!email || !password)
	{
		return res.status(400).send({
			error: "Check credentials"
		})
	}

	User.findOne({email})
	.then(user=>
	{
		if(!user)
		{
			return res.status(400).send({
				error: "Check credentials"
			})
		}
		else
		{
			bcrypt.compare(password,user.password).then(function(result) 
			{
			    console.log(result);
			    if(result)
			    {
			    	let {_id, fullname,email, isAdmin} = user;
			    	let token = jwt.sign({_id: user,_id}, 'secret_key')
						res.send({
							message: "Login successful",
							token,
							user: {
								_id,
								fullname,
								email,
								isAdmin
							}
						})
			    }
			    else
			    {
			    	return res.status(400).send({
			    		error: "Check credentials"
			    	})
			    }
			})
		}
	})
})


router.get('/profile', 
	passport.authenticate('jwt',{session: false}), 
	(req,res,next)=>
{
	res.send(req.user);
})
module.exports = router;