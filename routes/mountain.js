const router = require ('express').Router();
// require the model for products to query to database
const Mountain = require('./../models/Mountain');
const multer = require('multer');
const passport = require('passport');

// set multer settings
	const storage = multer.diskStorage({
	  destination: function (req, file, cb) {
	    cb(null, 'assets/images')
	  },
	  filename: function (req, file, cb) {
	  	// console.log(file)
	    cb(null, Date.now() + "-" + file.originalname)
	  }
	})
 
	const upload = multer({ storage: storage })

const adminOnly = (req,res,next)=>{
	if(req.user.isAdmin){
		next()
	}else{
		res.status(403).send({
			error:"Forbidden"
		})
	}
}
// const tester = (req,res,next) =>{
// 	console.log("this is just a test")
// 	next()
// }
// router level middleware
router.use((req,res,next)=>{
	// console.log(req);
	// console.log(req.method + " " + req.originalUrl + " " + new Date(Date.now()) )
	console.log(req.ip + " " + req.originalUrl + " " + new Date(Date.now()) )
	next()
})



router.get('/', (req, res)=> {
	Mountain.find()
	.then(mountains=>res.send(mountains))
	
})

router.get('/:id', (req, res,next)=>{
	Mountain.findById(req.params.id)
	.then(mountain=> res.send(mountain))
	.catch(next)
})

router.post('/', 
	passport.authenticate('jwt', {session:false}),
	adminOnly,
	upload.single('image'), 
	(req, res,next) =>{
		console.log(req.body)
		// console.log(req.body); add body parser(express.json()) in app.js to get the request body
		// console.log(req.file.filename)
		req.body.image = "public/" + req.file.filename;
		Mountain.create(req.body)
			.then(mountain => res.send(mountain))
			.catch(next)
})



router.put('/:id',
	passport.authenticate('jwt', {session:false}),
	adminOnly, 
	upload.single('image'), 
	(req, res,next)=> {
		if(req.file){
			req.body.image = "public/" + req.file.filename;
		}
		Mountain.findByIdAndUpdate(
			req.params.id,
			req.body,
			{new:true}
		)
		.then(mountain=> res.send(mountain))
		.catch(next)
})

router.delete('/:id',
	passport.authenticate('jwt', {session:false}),
	adminOnly,
	(req, res,next)=>{
		Mountain.findByIdAndDelete(req.params.id)
		.then(mountain => res.send({
			mountain,
			message: "Product has been deleted"
		}))
		.catch(next)
})

module.exports = router;