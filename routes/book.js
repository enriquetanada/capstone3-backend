const router = require('express').Router();
const Mountain = require('./../models/Mountain');

router.post('/', (req,res,next)=>{
	const arrayOfIds = req.body.orders.map(order =>{
		return order.mountainId
	})

	Mountain.find({_id: {$in: arrayOfIds}})
	.then(mountains=>{
		let total =0;
		let orderListSummary = mountains.map(mountain=>{
			let matchedMountain = {};
			req.body.orders.forEach(order =>{
				if(order.mountainId == mountain._id)
				{
					matchedMountain = 
					{
						mountainId: mountain._id,
						rate: mountain.rate,
						name: mountain.name,
						person:order.person,
						subtotal: order.person*mountain.rate
					}
					if(order.person >=6){
						total += matchedMountain.subtotal - (matchedMountain.subtotal *.20);
					} else{
						total += matchedMountain.subtotal
					}
					
				}
			})
			return matchedMountain;		
		})
		res.send({orders : orderListSummary, total});
	})
})

module.exports = router;