const router = require('express').Router();
const Reservation = require('./../models/Reservation');
const passport = require('passport');
const Mountain = require('./../models/Mountain');


const authenticate = passport.authenticate('jwt', {session:false});
router.get('/', authenticate, (req,res,next)=>{

	let filter = {}
	if(!req.user.isAdmin)
	{
		filter = {customerId: req.user._id}
	}

	Reservation.find(filter)
	.populate('customerId',['email','fullname'])
	.populate('orders.mountainId',['name', 'image', 'description', 'information', 'itinerary'])
	.then(reservations=>{
		res.send(reservations)
	})
	.catch(next)
})

router.get('/:id',authenticate, (req,res,next)=>{

	let filter = {_id: req.params.id}

	if(!req.user.isAdmin){
		filter = 
		{
			...filter,
			customerId: req.user._id
		}
	}

	Reservation.findOne(filter)
	.populate('customerId',['email','fullname'])
	.populate('orders.mountainId',['name', 'image', 'description', 'information', 'itinerary'])
	.then(reservation=>{
		if(reservation){
			res.send(reservation)
		}
		else
		{
			res.status(403).send("Unauthorize")
		}
	})
	.catch(next)
})

router.post('/',authenticate , (req, res, next)=>{

	let customerId = req.user._id

	const arrayOfIds = req.body.orders.map(order =>{
	return order.mountainId
	})

	Mountain.find({_id: {$in: arrayOfIds}})
	.then(mountains=>{
		let total =0;
		let orderListSummary = mountains.map(mountain=>{
			let matchedMountain = {};
			req.body.orders.forEach(order =>{
			if(order.mountainId == mountain._id)
				{
					matchedMountain = 
					{
						mountainId: mountain._id,
						rate: mountain.rate,
						name: mountain.name,
						person:order.person,
						subtotal: order.person*mountain.rate
					}
					if(order.person >=6){
						total += matchedMountain.subtotal - (matchedMountain.subtotal *.20);
					} else{
						total += matchedMountain.subtotal
					}
				}
			})
			return matchedMountain;
				
		})
			Reservation.create({
				customerId,
				orders : orderListSummary, total
			})
			.then(reservation =>{
				res.send(reservation)
			})
			.catch(next)
	})
})

router.put('/:id',authenticate, (req,res,next)=>{
	req.user.isAdmin ? next() : res.status(403).send("Unauthorize")
	} ,
	(req,res,next)=>{
	Reservation.findByIdAndUpdate(
		req.params.id,
		{isComplete: req.body.isComplete},
		{new:true}
	)
	.then(reservation => res.send(reservation))
	.catch(next)
})

module.exports = router;