const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express()


const port = process.env.PORT || 5000

const mountains= require('./routes/mountain');
const users = require('./routes/user');
const reservations = require('./routes/reservation');
const books = require('./routes/book');

require('dotenv').config();



// mongoose.connect('mongodb://localhost:27017/mountains_db', {
mongoose.connect(process.env.ATLAS, {

	useNewUrlParser: true, 
	useUnifiedTopology: true,
	useCreateIndex: true,
	useFindAndModify:false
});

mongoose.connection.on('connected', ()=>{
	console.log("Connected to database")
});


app.use((req, res, next)=>{
	console.log(req.method);
	next();
})

app.use(express.json());
app.use(cors());

app.use('/public', express.static('assets/images'))

app.use('/mountains', mountains)
app.use('/users', users)
app.use('/reservations', reservations)
app.use('/books', books)


app.get('/', (req, res) => {
  res.send('Hello World!')
})


app.use((err,req,res,next)=>{
	console.log(err.message)
	res.status(400).send({
		error:err.message
	})

})

app.listen(port, () => {
  console.log(`App is listening at port ${port}`)
})